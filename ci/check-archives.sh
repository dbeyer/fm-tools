#!/bin/bash

# Enable exit on error
set -e

COMPETITION="$1"
YEAR="$2"

if [[ -z "$COMPETITION" || -z "$YEAR" ]]; then
  echo "Usage: $0 <competition> <year>"
  echo "Example: $0 svcomp 2024"
  exit 1
fi

COMP_VERSION="$COMPETITION${YEAR#[0-9][0-9]}"

echo "Check all tool files that have a version $COMP_VERSION with an archive DOI."

echo
echo "The following tools have a version $COMP_VERSION with DOI:"
TOOLS[$COMP_VERSION]=$(for TOOLFILE in data/*.yml; do
    TOOL=$(basename "$TOOLFILE" ".yml")
    yq --raw-output ".versions [] | select(.version == \"$COMP_VERSION\" and .doi != null) | \"$TOOL\"" "$TOOLFILE"
done)
echo "${TOOLS["$COMP_VERSION"]}"

echo
echo "The following tool files were changed:"
if [ -n "$CI_MERGE_REQUEST_SOURCE_BRANCH_NAME" ]; then
  SOURCE_BRANCH="origin/$CI_MERGE_REQUEST_SOURCE_BRANCH_NAME"
  TARGET_BRANCH="origin/$CI_MERGE_REQUEST_TARGET_BRANCH_NAME"
else
  SOURCE_BRANCH="main"
  TARGET_BRANCH="origin/main"
fi
MERGE_BASE_COMMIT=$(git merge-base "$SOURCE_BRANCH" "$TARGET_BRANCH")
DIFF_FILES=$(git diff --name-only --diff-filter=d "$MERGE_BASE_COMMIT..$SOURCE_BRANCH" -- data/*.yml)
echo "$DIFF_FILES"

echo
echo "Check changed tool files."
for TOOLFILE in $DIFF_FILES; do
    TOOL=$(basename "$TOOLFILE" ".yml")
    if [ -n "$(yq --raw-output ".versions [] | select(.version == \"$COMP_VERSION\") | \"$TOOL\"" "$TOOLFILE")" ]; then
        echo "INFO: Tool '$TOOL' has a version '$COMP_VERSION'."
        DOI=$(yq --raw-output ".versions [] | select(.version == \"$COMP_VERSION\") | .doi" "$TOOLFILE")
        if [[ "$DOI" =~ ^"10.5281/zenodo." ]]; then
            echo "INFO: Tool archive for '$TOOL' with version '$COMP_VERSION' has DOI: '$DOI'."
        else
            echo "ERROR: Tool archive for '$TOOL' with version '$COMP_VERSION' has an invalid DOI: '$DOI'." && false
        fi
    fi
    if [[ ${TOOLS[$COMP_VERSION]} =~ $TOOL ]]; then
        echo "Check archive of $TOOL for competition $COMP_VERSION."
        TOOL_DIR=$(mktemp --directory --tmpdir "check-archives-$TOOL-XXXXXXXXXX")
        mkdir "$TOOL_DIR"/cache
        mkdir "$TOOL_DIR/$YEAR"

        echo "Downloading archive for $TOOL into $TOOL_DIR ..."
        "$(dirname "$0")"/../scripts/execute_runs/update-archives.py \
            --fm-root "$(dirname "$0")"/../data/ \
            --archives-root "$TOOL_DIR" \
            --competition "$COMPETITION" \
            --year "$YEAR" \
            "$TOOL"

        echo "Checking archive for $TOOL in $TOOL_DIR for $COMP_VERSION ..."
        "$(dirname "$0")"/../scripts/test/check-archive.py "$COMPETITION" "$TOOL_DIR/$YEAR/$TOOL.zip"

        rm "$TOOL_DIR/$YEAR/$TOOL.zip"
        rmdir "$TOOL_DIR/$YEAR"
        rm "$TOOL_DIR"/cache/[0-9]*[0-9].zip
        rmdir "$TOOL_DIR"/cache
        rmdir "$TOOL_DIR"
    fi
done
