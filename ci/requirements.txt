argcomplete>=1.9.4
black>=23.1.0
httpx[http2]>=0.25.0
jsonschema>=4.19.2
pandas>=1.5.2
progressbar2>=4.2.0
pyyaml>=5.4.1
yq>=3.1.0
